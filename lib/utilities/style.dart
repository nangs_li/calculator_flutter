import 'package:flutter/material.dart';

const rocheBlueColor = Color(0xFF1C71D7);
const rocheBlackColor = Color(0XFF020003);
const lightGreyBKGColor = Color(0xFFF8F5F9);
const rocheWhiteColor = Color(0xFFFFFFFF);

const blackBoldTitle = TextStyle(
  color: Color(0xff020003),
  fontSize: 16,
  fontWeight: FontWeight.w700,
);

const greySubtitle = TextStyle(
  color: Color(0xff848085),
  fontSize: 13,
  fontWeight: FontWeight.w400,
);

const blueSerifTitle = TextStyle(
  color: rocheBlueColor,
  fontSize: 24,
  fontFamily: 'Minion',
);

const bottomNavbarTitle =
TextStyle(fontSize: 10, color: Colors.black54, fontWeight: FontWeight.bold);

const learnPageListViewHeader = TextStyle(
  color: Color.fromARGB(255, 171, 171, 171),
  fontSize: 14,
  letterSpacing: 0.2,
  fontFamily: "SF Pro Text",
);

const learnPageListViewText = TextStyle(
  color: Color.fromARGB(255, 7, 7, 7),
  fontSize: 18,
  letterSpacing: -0.4,
  fontFamily: "SF Pro Text",
);

const learnPageBigHeader = TextStyle(
  color: Color.fromARGB(255, 7, 7, 7),
  fontSize: 32,
  letterSpacing: -0.2,
  fontFamily: "SF Pro Display",
  fontWeight: FontWeight.w700,
);

const faqPageText = TextStyle(
  color: Color.fromARGB(255, 104, 104, 104),
  fontSize: 14,
  letterSpacing: -0.2,
  fontFamily: "SF Pro Text",
);

final Image searchIcon =  Image.asset(
  "assets/images/search_icon.png",
  fit: BoxFit.scaleDown,
  height: 24,
);

final Image feedbackIcon =  Image.asset(
  "assets/images/-icon-2.png",
);

final Duration changeAnimationSearchBarDuration = Duration(milliseconds: 50);
final Duration searchBarAnimationDuration = Duration(milliseconds: 200);
final Duration searchBarAnimationTotalFinishDuration = Duration(milliseconds: 250);


const String defaultFontFamily = "OpenSans-Regular";
const String defaultBoldFontFamily = "OpenSans-Bold";
const Color DefaultBlueTextColor = Color.fromRGBO(14, 54, 78, 1);
const Color dividerColor = Color.fromRGBO(230, 230, 230, 1);
const Color calculateButtonColor = Color.fromRGBO(17, 136, 201, 1);
const Color OrangeColor = Color.fromRGBO(252, 43, 7, 1);
const Color TopTextBackGroundGreyColor = Color.fromRGBO(228, 228, 229, 1);
const Color DoseCardTopDivider = Color.fromRGBO(16, 135, 200, 1);
const Color BottomButtonColor = Color(0xff029BD3);
const Color BlueWarningColor = Color.fromRGBO(36, 136, 201, 1);

const BoldTextStyle = TextStyle(
  color: DefaultBlueTextColor,
  fontFamily: defaultBoldFontFamily,
  fontWeight: FontWeight.w700,
  fontSize: 20.0,
);