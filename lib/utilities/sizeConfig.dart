import 'package:flutter/cupertino.dart';

class SizeConfig {
  static MediaQueryData mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;

  static double safeAreaHorizontal;
  static double safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  //reponse width rate from device width /iPhone 11 Prox Max width
  //reponse height rate from device height /iPhone 11 Prox Max width
  static double responseWidthRate;
  static double responseHeightRate;
  static double responseFrontSizeRate;

  static double leftPadding;
  static double insideLeftPadding;

  static final SizeConfig _instance = SizeConfig._internal();

  factory SizeConfig() {
    return _instance;
  }

  SizeConfig._internal() {
    // init things inside this
  }

  void init(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    screenWidth = mediaQueryData.size.width;
    screenHeight = mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    safeAreaHorizontal = mediaQueryData.padding.left +
        mediaQueryData.padding.right;
    safeAreaVertical = mediaQueryData.padding.top +
        mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth -
        safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight -
        safeAreaVertical) / 100;

    //https://www.paintcodeapp.com/news/ultimate-guide-to-iphone-resolutions
//base on iphone 11 Pro Max
    responseWidthRate = ((screenWidth - safeAreaHorizontal) / 414);
    responseHeightRate = ((screenHeight - safeAreaVertical) / 896);
    responseFrontSizeRate = responseWidthRate;
    print("screenWidth: $screenWidth");
    print("screenHeight: $screenHeight");
    print("blockSizeHorizontal: $blockSizeHorizontal");
    print("blockSizeVertical: $blockSizeVertical");
    print("_safeAreaHorizontal: $safeAreaHorizontal");
    print("_safeAreaVertical: $safeAreaVertical");
    print("safeBlockHorizontal: $safeBlockHorizontal");
    print("safeBlockVertical: $safeBlockVertical");
    print("responseWidthRate: $responseWidthRate");
    print("responseHeightRate: $responseHeightRate");
    print("responseFrontSizeRate: $responseFrontSizeRate");


    leftPadding = 15.0 * SizeConfig.responseWidthRate;
    print("leftPadding: $leftPadding");
    insideLeftPadding = 30.0 * SizeConfig.responseWidthRate;
    print("insideLeftPadding: $insideLeftPadding");


  }
}