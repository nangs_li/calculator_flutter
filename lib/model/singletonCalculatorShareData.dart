import 'package:calculator/view/doseImageCard.dart';
import 'package:csv/csv.dart';
import 'package:flutter/services.dart';

class SingletonCalculatorShareData {
  static final SingletonCalculatorShareData _instance = SingletonCalculatorShareData._internal();

  factory SingletonCalculatorShareData() {
    return _instance;
  }

  // excels data
  List<List<dynamic>> maintenanceExcelList = [];
  List<List<dynamic>> loadingExcelList = [];
  List oneWeeksMaintenanceExcelList = List();
  List twoWeeksMaintenanceExcelList = List();
  List fourWeeksMaintenanceExcelList = List();
  List oneWeeksLoadingExcelList = List();
  // list of doseType
  List oneWeeksMaintenanceDoseTypeList = List();
  List twoWeeksMaintenanceDoseTypeList = List();
  List fourWeeksMaintenanceDoseTypeList = List();
  List oneWeeksLoadingDoseTypeList = List();

  var kg;
  bool selectedCard = false;
  int selectedCardIndex = 0;
  bool allClearData = false;
  bool isMaintenanceDose = true;
  List<String> vialsWeeksStringList ;
  String dropdownValue;

  SingletonCalculatorShareData._internal() {
    // init things inside this
  }

  bool showWarning() {
    bool showWarning = false;
    List currentSelectedDoseTypeList = getCurrentSelectedDoseTypeList();
    if (currentSelectedDoseTypeList.length >= 2) {
      for (var i = 0; i < currentSelectedDoseTypeList.length; i++) {
        DoseType type =  currentSelectedDoseTypeList[i];
       if(type == DoseType.SkyBlue){
         showWarning = true;
       }
      }
    }
    return showWarning;
  }

  List getCurrentSelectedDoseTypeList() {
    List currentSelectedDoseTypeList = oneWeeksMaintenanceDoseTypeList;
    if(!isMaintenanceDose) {
      currentSelectedDoseTypeList = oneWeeksLoadingDoseTypeList;
    } else if(selectedCardIndex == 1){
      currentSelectedDoseTypeList = oneWeeksMaintenanceDoseTypeList;
    } else if(selectedCardIndex == 2){
      currentSelectedDoseTypeList = twoWeeksMaintenanceDoseTypeList;
    } else if(selectedCardIndex == 3){
      currentSelectedDoseTypeList = fourWeeksMaintenanceDoseTypeList;
    }
    return currentSelectedDoseTypeList;
  }

  List<int> getCurrentVialsDoseTypeList() {
    List<int> currentVialsDoseTypeList = new List<int>();
    List selectWeekDataList = getSelectWeekDataList();
    if (dropdownValue == '2 weeks') {
      for (var j = 47; j < 51; j++) {
        currentVialsDoseTypeList.add(selectWeekDataList[j]);
      }
    } else if (dropdownValue == '4 weeks') {
      for (var j = 51; j < 55; j++) {
        currentVialsDoseTypeList.add(selectWeekDataList[j]);
      }
    } else if (dropdownValue == '6 weeks') {
      for (var j = 55; j < 59; j++) {
        currentVialsDoseTypeList.add(selectWeekDataList[j]);
      }
    } else if (dropdownValue == '8 weeks') {
      for (var j = 59; j < 63; j++) {
        currentVialsDoseTypeList.add(selectWeekDataList[j]);
      }
    } else if (dropdownValue == '10 weeks') {
      for (var j = 63; j < 67; j++) {
        currentVialsDoseTypeList.add(selectWeekDataList[j]);
      }
    } else if (dropdownValue == '12 weeks') {
      for (var j = 67; j < 71; j++) {
        currentVialsDoseTypeList.add(selectWeekDataList[j]);
      }
    }
    return currentVialsDoseTypeList;
  }

  String getSelectedWeek(){
    String weeks = "QW";
    if(selectedCardIndex == 1){
      weeks = "QW";
    } else if(selectedCardIndex == 2){
      weeks = "Q2W";
    } else if(selectedCardIndex == 3){
      weeks = "Q4W";
    }
    return weeks;
  }

  List getSelectWeekDataList() {
    List excelList;

    if(!isMaintenanceDose) {
      excelList = oneWeeksLoadingExcelList;
    } else if(selectedCardIndex == 1) {
      excelList = oneWeeksMaintenanceExcelList;
    } else if (selectedCardIndex == 2){
      excelList = twoWeeksMaintenanceExcelList;
    } else if (selectedCardIndex == 3){
      excelList = fourWeeksMaintenanceExcelList;
    }
    return excelList;
  }

  String get30mgDrugName(List weeksExcelList) {
    for(var i = 0; i < weeksExcelList.length; i++){
      if(i>=6 && i<=9){
        if(weeksExcelList[i] != ""){
          return "Sky Blue";
          //"30 mg/1.0 mL";
        }
      }
    }
    return null;
  }

  double get30mgVolume(List weeksExcelList) {
    double volume = 0;
    if (weeksExcelList[6] != "") {
      volume += weeksExcelList[6];
    }
    if (weeksExcelList[8] != "") {
      volume += weeksExcelList[8];
    }
    return volume;
  }

  String get30mgDrugML(List weeksExcelList) {
    if (weeksExcelList[6] != "") {
      return "Full bottle X${(weeksExcelList[6]).toInt()}";
    }
    if (weeksExcelList[8] != "") {
      return "${weeksExcelList[9]} mL X${(weeksExcelList[8]).toInt()}";
    }
    return "0.0 mL";
  }

  String get60mgDrugName(List weeksExcelList) {
    for(var i = 0; i < weeksExcelList.length; i++){
      if(i>=10 && i<=13){
        if(weeksExcelList[i] != ""){
          return "Purple";
          //"60 mg/0.4 mL";
        }
      }
    }
    return null;
  }

  double get60mgVolume(List weeksExcelList) {
    double volume = 0;
    if (weeksExcelList[10] != "") {
      volume += weeksExcelList[10];
    }
    if (weeksExcelList[12] != "") {
      volume += weeksExcelList[12];
    }
    return volume;
  }

  String get60mgDrugML(List weeksExcelList) {
    if (weeksExcelList[10] != "") {
      return "Full bottle X${(weeksExcelList[10]).toInt()}";
    }
    if (weeksExcelList[12] != "") {
      return "${weeksExcelList[13]} mL X${(weeksExcelList[12]).toInt()}";
    }
    return "0.0 mL";
  }

  String get105mgDrugName(List weeksExcelList) {
    for(var i = 0; i < weeksExcelList.length; i++){
      if(i>=14 && i<=17){
        if(weeksExcelList[i] != ""){
          return "Turquoise";
          //"105 mg/0.7 mL";
        }
      }
    }
    return null;
  }

  double get105mgVolume(List weeksExcelList) {
    double volume = 0;
    if (weeksExcelList[14] != "") {
      volume += weeksExcelList[14];
    }
    if (weeksExcelList[16] != "") {
      volume += weeksExcelList[16];
    }
    return volume;
  }

  String get105mgDrugML(List weeksExcelList) {
    if (weeksExcelList[14] != "") {
      return "Full bottle X${(weeksExcelList[14]).toInt()}";
    }
    if (weeksExcelList[16] != "") {
      return "${weeksExcelList[17]} mL X${(weeksExcelList[16]).toInt()}";
    }
    return "0.0 mL";
  }

  String get150mgDrugName(List weeksExcelList) {
    for(var i = 0; i < weeksExcelList.length; i++){
      if(i>=18 && i<=21){
        if(weeksExcelList[i] != ""){
          return "Brown";
          //"150 mg/1.0 mL";
        }
      }
    }
    return null;
  }

  double get150mgVolume(List weeksExcelList) {
    double volume = 0;
    if (weeksExcelList[18] != "") {
      volume += weeksExcelList[18];
    }
    if (weeksExcelList[20] != "") {
      volume += weeksExcelList[20];
    }
    return volume;
  }

  String get150mgDrugML(List weeksExcelList) {
    if (weeksExcelList[18] != "") {
      return "Full bottle X${(weeksExcelList[18]).toInt()}";
    }
    if (weeksExcelList[20] != "") {
      return "${weeksExcelList[21]} mL X${(weeksExcelList[20]).toInt()}";
    }
    return "0.0 mL";
  }

  double getTotalVolume(List weeksExcelList) {
    return weeksExcelList[26];
  }

  String getStringFromInjectionNumber(List weeksExcelList,int injectionNumber) {
    return (weeksExcelList[29] != "") ? "Injection $injectionNumber (${weeksExcelList[27 + injectionNumber * 2]}mL syringe)": null;
  }

  List<dynamic> getInjectionList(List weeksExcelList, int injectionNumber) {
    List<dynamic> list = new List<dynamic>();
    String instruction = weeksExcelList[28 + (injectionNumber * 2)];
    instruction = instruction.replaceAll(new RegExp('\n'), '');
    List instructionArray = instruction.split(";");
    if (instructionArray.last == "" || instructionArray.last == " ") {
      instructionArray.removeLast();
    }
    for (var i = 0; i < instructionArray.length; i++) {
      List instructionDetailArray = instructionArray[i].split(": ");
      if (instructionDetailArray[1] != null) {
        if (instructionDetailArray[1] == "sky_blue") {
          list.add(DoseType.SkyBlue);
          if (instructionDetailArray[0] == "1.0mL" ||
              instructionDetailArray[0] == " 1.0mL") {
            list.add("Full bottle");
          } else {
            list.add(instructionDetailArray[0]);
          }
        }
        if (instructionDetailArray[1] == "turquoise") {
          list.add(DoseType.Green);
          if (instructionDetailArray[0] == "0.70mL" ||
              instructionDetailArray[0] == " 0.70mL") {
            list.add("Full bottle");
          } else {
            list.add(instructionDetailArray[0]);
          }
        }
        if (instructionDetailArray[1] == "purple") {
          list.add(DoseType.Purple);
          if (instructionDetailArray[0] == "0.40mL" ||
              instructionDetailArray[0] == " 0.40mL") {
            list.add("Full bottle");
          } else {
            list.add(instructionDetailArray[0]);
          }
        }
        if (instructionDetailArray[1] == "brown") {
          list.add(DoseType.Brown);
          if (instructionDetailArray[0] == "1.0mL" ||
              instructionDetailArray[0] == " 1.0mL") {
            list.add("Full bottle");
          } else {
            list.add(instructionDetailArray[0]);
          }
        }
      }
    }
    print("getInjection$injectionNumber:$list");
    return list;
  }

  List<String> getVialsWeeksStringList(List weeksExcelList) {
    List<String> stringList = new List<String>();
    
    var isContain2Weeks = true;
    var isContain4Weeks = true;
    var isContain6Weeks = true;
    var isContain8Weeks = true;
    var isContain10Weeks = true;
    var isContain12Weeks = true;
      for (var j = 47; j < 51; j++) {
        if (weeksExcelList[j] == "") {
          isContain2Weeks = false;
        }
      }
    for (var j = 51; j < 55; j++) {
      if (weeksExcelList[j] == "") {
        isContain4Weeks = false;
      }
    }
    for (var j = 55; j < 59; j++) {
      if (weeksExcelList[j] == "") {
        isContain6Weeks = false;
      }
    }
    for (var j = 59; j < 63; j++) {
      if (weeksExcelList[j] == "") {
        isContain8Weeks = false;
      }
    }
    for (var j = 63; j < 67; j++) {
      if (weeksExcelList[j] == "") {
        isContain10Weeks = false;
      }
    }
    for (var j = 67; j < 71; j++) {
      if (weeksExcelList[j] == "") {
        isContain12Weeks = false;
      }
    }

    if (isContain2Weeks) {
      stringList.add('2 weeks');
    }
    if (isContain4Weeks) {
      stringList.add('4 weeks');
    }
    if (isContain6Weeks) {
      stringList.add('6 weeks');
    }
    if (isContain8Weeks) {
      stringList.add('8 weeks');
    }
    if (isContain10Weeks) {
      stringList.add('10 weeks');
    }
    if (isContain12Weeks) {
      stringList.add('12 weeks');
    }


    return stringList;
  }

  String getPrepareSyringeString(List weeksExcelList) {
    bool have39ColumnValue = false;
    String prepareSyringeString = "Prepare ";

    if (weeksExcelList[39] != "") {
      if(!weeksExcelList[39].toString().contains('0')){
      prepareSyringeString +=
          replaceDigitalNumberToEnglish(weeksExcelList[39].toStringAsFixed(0)) +
              " 1mL syringe";
      have39ColumnValue = true;
      }
    }

    if (weeksExcelList[40] != "") {
      if(!weeksExcelList[40].toString().contains('0')){
        if(have39ColumnValue){
          prepareSyringeString += ",\n";
        }
      prepareSyringeString +=
          replaceDigitalNumberToEnglish(weeksExcelList[40].toStringAsFixed(0)) +
              " 2mL syringe ";
      }
    }

    return prepareSyringeString;
  }

  bool isLeastWastageChoice(List weeksExcelList){
    bool isLeastWastageChoice = false;
    if (weeksExcelList[46] == 'Y') {
      isLeastWastageChoice = true;
    }
    return isLeastWastageChoice;
  }

  String replaceDigitalNumberToEnglish(String input) {
    const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const farsi = [
      'zero',
      'one',
      'two',
      'three',
      'four',
      'five',
      'six',
      'seven',
      'eight',
      'nine'
    ];

    for (int i = 0; i < english.length; i++) {
      input = input.replaceAll(english[i], farsi[i]);
    }

    return input;
  }

  //calculator part
  void getWeeksExcelList(kg) {

    this.kg = kg;
    var oneweeksIndex = kg + 3;
    var twoweeksIndex = kg + 154;
    var fourweeksIndex = kg + 304;
    print("oneweeksIndex: $oneweeksIndex");
    print("twoweeksIndex: $twoweeksIndex");
    print("fourweeksIndex: $fourweeksIndex");
    if(isMaintenanceDose) {
      oneWeeksMaintenanceExcelList =
      maintenanceExcelList[oneweeksIndex];
      print("oneweeks: $oneWeeksMaintenanceExcelList");
      twoWeeksMaintenanceExcelList =
      maintenanceExcelList[twoweeksIndex];
      print("twoWeeksExcelList $twoWeeksMaintenanceExcelList");
      fourWeeksMaintenanceExcelList =
      maintenanceExcelList[fourweeksIndex];
      print("fourweeks: $fourWeeksMaintenanceExcelList");
      print(kg * 1.5);
      print(kg * 3);
      print(kg * 6);
      oneWeeksMaintenanceDoseTypeList = new List();
      twoWeeksMaintenanceDoseTypeList = new List();
      fourWeeksMaintenanceDoseTypeList = new List();
      getCorrectDataIntoList(oneWeeksMaintenanceExcelList,1);
      getCorrectDataIntoList(twoWeeksMaintenanceExcelList,2);
      getCorrectDataIntoList(fourWeeksMaintenanceExcelList,3);
    }

    if(!isMaintenanceDose) {
      oneWeeksLoadingExcelList = loadingExcelList[oneweeksIndex];
      print("oneweeksLoadingExcelList: $oneWeeksLoadingExcelList");
      print("oneweeksIndex: $oneweeksIndex");
      print(kg * 3);
      getCorrectDataIntoList(oneWeeksLoadingExcelList,1);
    }

  }

  void getCorrectDataIntoList(List excelList,selectedCardIndex) {
    oneWeeksLoadingDoseTypeList = List();


    print("data start:");
    if(get30mgDrugName(excelList) != null) {
      print(get30mgDrugName(excelList));
      print("30 mg/1.0 mL");
      print(get30mgVolume(excelList).toStringAsFixed(0));
      print(get30mgDrugML(excelList));

      if(!isMaintenanceDose){
        oneWeeksLoadingDoseTypeList.add(DoseType.SkyBlue);
      } else if(selectedCardIndex == 1) {
        oneWeeksMaintenanceDoseTypeList.add(DoseType.SkyBlue);
      } else if(selectedCardIndex == 2) {
        twoWeeksMaintenanceDoseTypeList.add(DoseType.SkyBlue);
      } else if(selectedCardIndex == 3) {
        fourWeeksMaintenanceDoseTypeList.add(DoseType.SkyBlue);
      }

    }
    if(get60mgDrugName(excelList) != null) {
      print(get60mgDrugName(excelList));
      print("60 mg/0.4 mL");
      print(get60mgVolume(excelList).toStringAsFixed(0));
      print(get60mgDrugML(excelList));

      if(!isMaintenanceDose){
        oneWeeksLoadingDoseTypeList.add(DoseType.Purple);
      } else if(selectedCardIndex == 1) {
        oneWeeksMaintenanceDoseTypeList.add(DoseType.Purple);
      } else if(selectedCardIndex == 2) {
        twoWeeksMaintenanceDoseTypeList.add(DoseType.Purple);
      } else if(selectedCardIndex == 3) {
        fourWeeksMaintenanceDoseTypeList.add(DoseType.Purple);
      }
    }
    if(get105mgDrugName(excelList) != null) {
      print(get105mgDrugName(excelList));
      print("105 mg/0.7 mL");
      print(get105mgVolume(excelList).toStringAsFixed(0));
      print(get105mgDrugML(excelList));
      if(!isMaintenanceDose){
        oneWeeksLoadingDoseTypeList.add(DoseType.Green);
      } else if(selectedCardIndex == 1) {
        oneWeeksMaintenanceDoseTypeList.add(DoseType.Green);
      } else if(selectedCardIndex == 2) {
        twoWeeksMaintenanceDoseTypeList.add(DoseType.Green);
      } else if(selectedCardIndex == 3) {
        fourWeeksMaintenanceDoseTypeList.add(DoseType.Green);
      }
    }
    if(get150mgDrugName(excelList) != null) {
      print(get150mgDrugName(excelList));
      print("150 mg/1.0 mL");
      print(get150mgVolume(excelList).toStringAsFixed(0));
      print(get150mgDrugML(excelList));
      if(!isMaintenanceDose){
        oneWeeksLoadingDoseTypeList.add(DoseType.Brown);
      } else if(selectedCardIndex == 1) {
        oneWeeksMaintenanceDoseTypeList.add(DoseType.Brown);
      } else if(selectedCardIndex == 2) {
        twoWeeksMaintenanceDoseTypeList.add(DoseType.Brown);
      } else if(selectedCardIndex == 3) {
        fourWeeksMaintenanceDoseTypeList.add(DoseType.Brown);
      }
    }

    print(getTotalVolume(excelList).toStringAsFixed(2));
    print("oneWeeksMaintenanceList:$oneWeeksMaintenanceDoseTypeList");
    print("twoWeeksMaintenanceList:$twoWeeksMaintenanceDoseTypeList");
    print("fourWeeksMaintenanceList:$fourWeeksMaintenanceDoseTypeList");
    print("data end");

  }

  List<List<dynamic>> loadLoadingCSV() {

    loadAsset('assets/files/loading.csv').then((dynamic output) {
      this.loadingExcelList = const CsvToListConverter().convert(output);
      print ("rowsAsListOfValues:$loadingExcelList");

    });

    return [];

  }

  List<List<dynamic>> loadMaintenanceCSV() {

    loadAsset('assets/files/maintenance.csv').then((dynamic output) {
      this.maintenanceExcelList  = const CsvToListConverter().convert(output);
      print ("rowsAsListOfValues:$maintenanceExcelList");

    });

    return [];

  }

  Future<String> loadAsset(String path) async {
    return await rootBundle.loadString(path);
  }
}