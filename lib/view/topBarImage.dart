import 'package:calculator/model/singletonCalculatorShareData.dart';
import 'package:calculator/utilities/sizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum TopBarType { homePage, DosePage, InjectionPage }

class TopBarImage extends StatelessWidget implements PreferredSizeWidget {
  final TopBarType topBarType;

  const TopBarImage(this.topBarType);

  @override
  Size get preferredSize => Size.fromHeight(SizeConfig.safeBlockVertical * 16);

  Widget build(context) {
    SizeConfig().init(context);

    return Stack(fit: StackFit.loose, children: <Widget>[
      Container(
          width: SizeConfig.screenWidth - SizeConfig.leftPadding,
          height: SizeConfig.safeBlockVertical * 17.5,
          child: FittedBox(
            child: Image(image: AssetImage('assets/images/TopBarImage.png')),
            fit: BoxFit.fill,
          )),
      Padding(
          padding: EdgeInsets.only(
              top: (SizeConfig.safeBlockVertical * 17.5 -
                      50.0 * SizeConfig.responseHeightRate) /
                  2,
              left: 10 * SizeConfig.responseWidthRate),
          child: Row(children: <Widget>[
            new SizedBox(
                height: 50.0 * SizeConfig.responseHeightRate,
                width: 50.0 * SizeConfig.responseHeightRate,
                child: IconButton(
                  icon: Image.asset(
                    (topBarType == TopBarType.homePage)
                        ? "assets/images/Menu.png"
                        : "assets/images/Back.png",
                    fit: BoxFit.cover,
                  ),
                  onPressed: () {
                    if (topBarType == TopBarType.DosePage || topBarType == TopBarType.InjectionPage) {
                      Navigator.pop(context);
                    }
                  },
                )),
            homeImage(topBarType, context)
          ])),
    ]);
  }

  Widget homeImage(TopBarType topBarType, BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(
            left: 20 * SizeConfig.responseWidthRate),
        child: SizedBox(
            height: 50.0 * SizeConfig.responseHeightRate,
            width: 50.0 * SizeConfig.responseHeightRate,
            child: (topBarType == TopBarType.InjectionPage)
                ? IconButton(
                    icon: Image.asset(
                      "assets/images/Home.png",
                      fit: BoxFit.fitWidth,
                    ),
                    onPressed: () {
                      SingletonCalculatorShareData data = SingletonCalculatorShareData();
                      data.allClearData = true;
                      if(data.isMaintenanceDose) {
                        data.selectedCard = false;
                      }
                      Navigator.of(context).popUntil((route) => route.isFirst);
                    },
                  )
                : null));
  }
}
