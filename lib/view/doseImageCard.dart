import 'package:calculator/controller/baseViewController.dart';
import 'package:calculator/model/singletonCalculatorShareData.dart';
import 'package:calculator/utilities/sizeConfig.dart';
import 'package:calculator/utilities/style.dart';
import 'package:calculator/view/InjectionPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum DoseImageCardType { Dose, Injection , Vials}
enum DoseType {SkyBlue, Brown, Green,Purple}

class DoseImageCard extends StatelessWidget{
  final DoseImageCardType doseImageCardType;
  final String drugName;
  final String volume;
  final String drugML;
  final String imagePath;
  final int cardIndex;
  final String doseML;
  final String injectionNumberString;
  final double injectionTopPadding;

  const DoseImageCard({this.doseImageCardType,this.drugName,this.drugML,this.volume,this.imagePath,this.cardIndex,this.doseML,this.injectionNumberString,this.injectionTopPadding});


  Widget build(context) {
    return doseImageCardList(context,this.doseImageCardType);
  }

  Widget doseImageCardList(BuildContext context,DoseImageCardType doseImageCardType) {

    SizeConfig().init(context);

    Widget doseImageCardList;

  if(doseImageCardType == DoseImageCardType.Dose || doseImageCardType == DoseImageCardType.Vials){

    doseImageCardList =  Padding(
        padding: EdgeInsets.only(
            left: (doseImageCardType == DoseImageCardType.Vials) ? SizeConfig.insideLeftPadding + SizeConfig.leftPadding  : SizeConfig.insideLeftPadding ,
            right: (doseImageCardType == DoseImageCardType.Vials) ? SizeConfig.insideLeftPadding + SizeConfig.leftPadding : SizeConfig.insideLeftPadding ,
            top:  SizeConfig.insideLeftPadding / 3 ),
        child:
        Stack(fit: StackFit.loose, children: <Widget>[

          Image(image: AssetImage(
              imagePath),
              height:200 *
                  SizeConfig.responseWidthRate ,
              width: SizeConfig.screenWidth - SizeConfig.insideLeftPadding * 2 -
                  SizeConfig.leftPadding * 2,
              fit: BoxFit.contain),

          Padding(
              padding: EdgeInsets.only(
                  left: SizeConfig.leftPadding,
                  right: SizeConfig.leftPadding,
                  top: 80 * SizeConfig.responseHeightRate),
              child:
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(drugName,style:TextStyle(fontFamily: defaultFontFamily,color: Colors.white,fontSize: 18.0 *
                        SizeConfig.responseFrontSizeRate),textAlign: TextAlign.left),
                    Text(drugML,style:TextStyle(fontFamily: defaultFontFamily,color: Colors.white,fontSize: 12.0 *
                        SizeConfig.responseFrontSizeRate),textAlign: TextAlign.left),
                    Padding(
                        padding: EdgeInsets.only(
                            top: 8 * SizeConfig.responseHeightRate),
                        child:
                        Text(volume,style:TextStyle(fontFamily: defaultBoldFontFamily,color: Colors.white,fontWeight: FontWeight.w600,fontSize: 20.0 *
                            SizeConfig.responseFrontSizeRate),textAlign: TextAlign.left)
                    )
                  ])
          )
        ]
        )
    );
  } else if(doseImageCardType == DoseImageCardType.Injection) {

    double topPadding = (injectionNumberString == "") ?  SizeConfig.leftPadding : 70 ;

    doseImageCardList = Padding(
        padding: EdgeInsets.only(
            left: SizeConfig.leftPadding),

        child: Stack(fit: StackFit.loose, children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                top: injectionTopPadding * SizeConfig.responseHeightRate),
            child: TileText(injectionNumberString,
                15 * SizeConfig.responseHeightRate, 0),
          ),
          Padding(
              padding: EdgeInsets.only(
                  top: topPadding * SizeConfig.responseHeightRate),
              child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image(image: AssetImage(
                      imagePath),
                      height: 160 *
                          SizeConfig.responseWidthRate,
                      width: SizeConfig.screenWidth -
                          46 * SizeConfig.responseWidthRate -
                          SizeConfig.leftPadding * 3 -
                          SizeConfig.insideLeftPadding,
                      fit: BoxFit.contain))
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: SizeConfig.leftPadding,
                  right: SizeConfig.leftPadding,
                  top: (50 + topPadding + 20) * SizeConfig.responseHeightRate),
              child:
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(drugName, style: TextStyle(
                        fontFamily: defaultFontFamily,
                        color: Colors.white,
                        fontSize: 18.0 *
                            SizeConfig.responseFrontSizeRate),
                        textAlign: TextAlign.left),

                    Padding(
                        padding: EdgeInsets.only(
                            top: 8 * SizeConfig.responseHeightRate),
                        child:
                        Text(doseML, style: TextStyle(
                            fontFamily: defaultBoldFontFamily,
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 22.0 *
                                SizeConfig.responseFrontSizeRate),
                            textAlign: TextAlign.left)
                    )
                  ])
          )
        ]
        )
    );
  }

  return doseImageCardList;
  }
}
