import 'package:calculator/controller/baseViewController.dart';
import 'package:calculator/model/singletonCalculatorShareData.dart';
import 'package:calculator/utilities/sizeConfig.dart';
import 'package:calculator/utilities/style.dart';
import 'package:calculator/view/doseImageCard.dart';
import 'package:calculator/view/topBarImage.dart';
import 'package:flutter/material.dart';
import 'doseImageCardList.dart';
import 'dosePage.dart';


class VialsPage extends StatefulWidget {
  static const String routeName = "/dose";
  @override
  _VialsPageState createState() => _VialsPageState();
}

class _VialsPageState extends State<VialsPage> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    final data = SingletonCalculatorShareData();
    final List selectWeekDataList = data.getSelectWeekDataList();
    data.vialsWeeksStringList = data.getVialsWeeksStringList(selectWeekDataList);
    data.dropdownValue = data.vialsWeeksStringList[0];
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final InheritedContext inheritedContext = InheritedContext.of(context);
    final data = SingletonCalculatorShareData();
    var kg = data.kg;


    return Scaffold(
      backgroundColor: Colors.white,
      appBar: TopBarImage(TopBarType.InjectionPage),
      body: SafeArea(
        top: true,
        bottom: false,
        child: SingleChildScrollView(
    child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  left: SizeConfig.leftPadding,
                  right: SizeConfig.leftPadding,
                  top: 5 * SizeConfig.responseHeightRate),
              child: Container(
                  height: 60 * SizeConfig.responseHeightRate,
                  width: SizeConfig.screenWidth,
                  decoration: new BoxDecoration(
                      color: TopTextBackGroundGreyColor,
                      borderRadius: new BorderRadius.circular(3.0)),
                  child: Center(
                      child: Text(
                        'Maintenance Dose, $kg' +
                            'kg, ${data.getSelectedWeek()}',
                        style: TextStyle(
                          backgroundColor: TopTextBackGroundGreyColor,
                          color: DefaultBlueTextColor,
                          fontFamily: defaultBoldFontFamily,
                          fontWeight: FontWeight.w600,
                          fontSize: 20.0 * SizeConfig.responseFrontSizeRate,
                        ),
                        textAlign: TextAlign.center,
                      )
                  )
              ),
            ),
            Padding(
                padding: EdgeInsets.only(
                    top: 40 * SizeConfig.responseHeightRate,
                    bottom: 40 * SizeConfig.responseHeightRate),
                child: Center(
                    child: Text(
                      'Number of vials',
                      style: BoldTextStyle.copyWith(fontSize: 35 * SizeConfig.responseFrontSizeRate,color: OrangeColor),
                      textAlign: TextAlign.center,
                    )
                )
            ),
            Padding(
              padding: EdgeInsets.only(
                left: SizeConfig.leftPadding,
              ),
              child: Row(
                children: <Widget>[
                  Text(
                    'Period:',
                    style: BoldTextStyle.copyWith(
                        fontSize: 20 * SizeConfig.responseFrontSizeRate),
                    textAlign: TextAlign.left,
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                        left: SizeConfig.leftPadding,
                        right: SizeConfig.leftPadding
                      ),
                      child: Container(
                        width: 302.0 * SizeConfig.responseWidthRate,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(3),
                            border: Border.all(color: DefaultBlueTextColor,
                                width: 1.0)
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: SizeConfig.leftPadding,
                          ),
                          child: new DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                            value: data.dropdownValue,
                            icon: SizedBox(
                                height: 30.0 * SizeConfig.responseHeightRate,
                                width: 30.0 * SizeConfig.responseWidthRate,
                                child: IconButton(
                                  icon: Image.asset(
                                    "assets/images/BlueArrow.png",
                                    fit: BoxFit.fitWidth,
                                  ),

                                )),
                            iconSize: 24,
                            elevation: 16,
                            style: BoldTextStyle,
                            onChanged: (String newValue) {
                              setState(() {
                                data.dropdownValue = newValue;
                              });
                            },
                    items: data.vialsWeeksStringList
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    })
                        .toList(),
                  ),
                          ),
                  ),
                )
              )
              ],
            ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: SizeConfig.leftPadding,
                  right: SizeConfig.leftPadding * 5,
                  top: 35 * SizeConfig.responseHeightRate),
              child: Text(
                "Remaining of vials required:",
                style: TextStyle(
                  backgroundColor: Colors.white,
                  color: DefaultBlueTextColor,
                  fontFamily: defaultBoldFontFamily,
                  fontWeight: FontWeight.w500,
                  fontSize: 17.0 *
                      SizeConfig.responseFrontSizeRate,
                ),
              ),
            ),
            DoseImageCardList().createUI(DoseImageCardType.Vials),
            Padding(
              padding: EdgeInsets.only(
                  left: SizeConfig.insideLeftPadding + SizeConfig.leftPadding ,
                  right: SizeConfig.insideLeftPadding + SizeConfig.leftPadding,
                  top: SizeConfig.leftPadding + SizeConfig.leftPadding,
                  bottom:SizeConfig.insideLeftPadding + SizeConfig.leftPadding),
              child:
              ButtonTheme(
                minWidth: 500.0 * SizeConfig.responseWidthRate,
                height: 70.0 * SizeConfig.responseHeightRate,
                child: FlatButton(
                  onPressed: () {
                    SingletonCalculatorShareData data = SingletonCalculatorShareData();
                    data.allClearData = true;
                    Navigator.of(context).popUntil((route) => route.isFirst);
                },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                          height: 50.0 * SizeConfig.responseHeightRate,
                          width: 50.0 * SizeConfig.responseHeightRate,
                          child: IconButton(
                            icon: Image.asset(
                              "assets/images/BlueHome.png",
                              fit: BoxFit.fitWidth,
                            ),

                          ) ),
                      Text('Start Over',
                        style: TextStyle(
                          fontSize: 18 * SizeConfig.responseFrontSizeRate,
                            fontFamily: defaultBoldFontFamily,
                            fontWeight: FontWeight.w600,
                            color: BottomButtonColor),
                        textAlign: TextAlign.center,),
                    ],
                  ),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(side: BorderSide(color: BottomButtonColor)),
                ),
              ),
            ),
          ]
    ),
        ),
      ));
  }


}
