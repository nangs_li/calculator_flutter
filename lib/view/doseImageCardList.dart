import 'package:calculator/model/singletonCalculatorShareData.dart';
import 'package:flutter/cupertino.dart';

import 'doseImageCard.dart';

class DoseImageCardList {

  Widget createUI(DoseImageCardType type,{String weeksString}) {

    SingletonCalculatorShareData data = SingletonCalculatorShareData();
    List<Widget> widgetList = new List<Widget>();
    if(type == DoseImageCardType.Vials){
      List currentVialsDoseTypeList = data.getCurrentVialsDoseTypeList();
      for (var i = 0; i < currentVialsDoseTypeList.length; i++) {
        if (currentVialsDoseTypeList[i] != 0 && i == 0) {
          widgetList.add(DoseImageCard(
              doseImageCardType:DoseImageCardType.Vials,
              drugName:"Sky blue",
              drugML:"30 mg/1 mL(30 mg/mL)",
              volume:'X${currentVialsDoseTypeList[i].toString()}',
              imagePath:"assets/images/Skyblue_card1.png",
              cardIndex:1));
        }
        if (currentVialsDoseTypeList[i] != 0 && i == 1) {
          widgetList.add(DoseImageCard(
              doseImageCardType:DoseImageCardType.Vials,
              drugName:"Purple",
              drugML:"60 mg/0.4 mL(150 mg/mL)",
              volume:'X${currentVialsDoseTypeList[i].toString()}',
              imagePath:"assets/images/Purple_card1.png",
              cardIndex:2));
        }
        if (currentVialsDoseTypeList[i] != 0 && i == 2) {
          widgetList.add(DoseImageCard(
              doseImageCardType:DoseImageCardType.Vials,
              drugName:"Turquoise",
              drugML:"105 mg/0.7 mL(150 mg/mL)",
              volume:'X${currentVialsDoseTypeList[i].toString()}',
              imagePath:"assets/images/Green_card1.png",
              cardIndex:i));
        }
        if (currentVialsDoseTypeList[i] != 0 && i == 3) {
          widgetList.add(DoseImageCard(
              doseImageCardType:DoseImageCardType.Vials,
              drugName:"Brown",
              drugML:"150 mg/1 mL(150 mg/mL)",
              volume:'X${currentVialsDoseTypeList[i].toString()}',
              imagePath:"assets/images/Brown_card1.png",
              cardIndex:i));
        }
      }
    } else {
      List<dynamic> excelList = data.getSelectWeekDataList();
      List doseTypeList = data.getCurrentSelectedDoseTypeList();
      if (doseTypeList == null) {
        return null;
      }
      for (var i = 0; i < doseTypeList.length; i++) {
        if (doseTypeList[i] == DoseType.SkyBlue) {
          widgetList.add(DoseImageCard(
              doseImageCardType:DoseImageCardType.Dose,
              drugName:"Sky blue",
              drugML:"30 mg/1 mL(30 mg/mL)",
              volume:"X" + data.get30mgVolume(
                  excelList).toStringAsFixed(0),
              imagePath:"assets/images/Skyblue_card1.png",
              cardIndex:i,
              doseML:data.get30mgDrugML(excelList)));
        } else if (doseTypeList[i] == DoseType.Purple) {
          widgetList.add(DoseImageCard(
              doseImageCardType:DoseImageCardType.Dose,
              drugName:"Purple",
              drugML:"60 mg/0.4 mL(150 mg/mL)",
              volume:"X" + data.get60mgVolume(
                  excelList).toStringAsFixed(0),
              imagePath:"assets/images/Purple_card1.png",
              cardIndex:i,
              doseML:data.get60mgDrugML(excelList)),);
        } else if (doseTypeList[i] == DoseType.Green) {
          widgetList.add(DoseImageCard(
              doseImageCardType:DoseImageCardType.Dose,
              drugName:"Turquoise",
              drugML:"105 mg/0.7 mL(150 mg/mL)",
              volume:"X" + data.get105mgVolume(
                  excelList).toStringAsFixed(0),
              imagePath:"assets/images/Green_card1.png",
              cardIndex:i,
              doseML:data.get105mgDrugML(excelList)));
        } else if (doseTypeList[i] == DoseType.Brown) {
          widgetList.add(DoseImageCard(
              doseImageCardType:DoseImageCardType.Dose,
              drugName:"Brown",
              drugML:"150 mg/1 mL(150 mg/mL)",
              volume:"X" + data.get150mgVolume(
                  excelList).toStringAsFixed(0),
              imagePath:"assets/images/Brown_card1.png",
              cardIndex:i,
              doseML:data.get150mgDrugML(excelList)));
        }
      }
    }
    return new Column(children: widgetList);

  }
}