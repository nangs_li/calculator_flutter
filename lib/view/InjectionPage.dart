import 'package:calculator/controller/baseViewController.dart';
import 'package:calculator/model/singletonCalculatorShareData.dart';
import 'package:calculator/utilities/fade_route.dart';
import 'package:calculator/utilities/sizeConfig.dart';
import 'package:calculator/utilities/style.dart';
import 'package:calculator/view/topBarImage.dart';
import 'package:calculator/view/vialsPage.dart';
import 'package:flutter/material.dart';
import 'doseImageCard.dart';


class InjectionPage extends StatefulWidget {
  static const String routeName = "/dose";
  @override
  _InjectionPageState createState() => _InjectionPageState();
}

class _InjectionPageState extends State<InjectionPage> {

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final InheritedContext inheritedContext = InheritedContext.of(context);
    final data = SingletonCalculatorShareData();
    var kg = data.kg;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: TopBarImage(TopBarType.InjectionPage),
      body: SafeArea(
        top: true,
        bottom: false,
        child: SingleChildScrollView(
    child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  left: SizeConfig.leftPadding, right: SizeConfig.leftPadding,top: 5 * SizeConfig.responseHeightRate),
              child: Container(
                  height: 60 * SizeConfig.responseHeightRate,
                  width: SizeConfig.screenWidth,
                  decoration: new BoxDecoration(
                      color: TopTextBackGroundGreyColor,
                      borderRadius: new BorderRadius.circular(3.0)),
                  child: Center(
                      child: Text(
                        data.isMaintenanceDose ? 'Maintenance Dose, $kg' + 'kg, ${data.getSelectedWeek()}' : 'Loading Dose, $kg' + 'kg, QW'  ,
                        style: TextStyle(
                          backgroundColor: TopTextBackGroundGreyColor,
                          color: DefaultBlueTextColor,
                          fontFamily: defaultBoldFontFamily,
                          fontWeight: FontWeight.w600,
                          fontSize: 20.0 * SizeConfig.responseFrontSizeRate,
                        ),
                        textAlign: TextAlign.center,
                      )
                  )
              ),
            ),
        Padding(
          padding: EdgeInsets.only(
              top: 40 * SizeConfig.responseHeightRate ,bottom: 20 * SizeConfig.responseHeightRate),
          child: Center(
                child: Text(
                  'Volume of injection' ,
                  style: BoldTextStyle.copyWith(fontSize: 35 * SizeConfig.responseFrontSizeRate,color: OrangeColor),
                  textAlign: TextAlign.center,
                )
            )
        ),
            (data.showWarning()) ? Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.leftPadding,
              right: SizeConfig.leftPadding),
              child: new Container(
                height: 210 * SizeConfig.responseHeightRate,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black,width: 2),
                    color: BlueWarningColor,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[

                      SizedBox(
                          height: 80.0 * SizeConfig.responseWidthRate,
                          width: 80.0 * SizeConfig.responseWidthRate,
                          child: Image.asset(
                            "assets/images/Warning.png",
                            fit: BoxFit.fitWidth,
                          ))

                      , Expanded(
                          child: Padding(
                              padding: EdgeInsets.only(
                                top: 15 * SizeConfig.responseHeightRate , bottom: 15 * SizeConfig.responseHeightRate,right: SizeConfig.insideLeftPadding
                              ),
                              child:Text(
                            "Do not combine two different concentrations of HEMLIBRA® (30 mg/mL and 150 mg/mL) in a single injection.",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 5, style: TextStyle(
                            color: Colors.white,
                            fontFamily: defaultBoldFontFamily,
                            fontWeight: FontWeight.w600,
                            fontSize: 20.0 *
                                SizeConfig.responseFrontSizeRate,
                                  wordSpacing: 0.0
                              ),
                          )
                          )
                      )
                    ]
                )
            )
            ) : SizedBox(),
            Padding(
                padding: EdgeInsets.only(
                    top: 30 * SizeConfig.responseHeightRate,
                    left: SizeConfig.leftPadding,
                    right: SizeConfig.leftPadding),
                child:
                Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      doseImageCardList()
                    ]
                )
            ),
            (!data.isMaintenanceDose) ? Container() : Padding(
              padding: EdgeInsets.only(
                  left: SizeConfig.insideLeftPadding + SizeConfig.leftPadding,
                  right: SizeConfig.insideLeftPadding + SizeConfig.leftPadding,
                  top: SizeConfig.leftPadding,
                  bottom:SizeConfig.insideLeftPadding + SizeConfig.leftPadding),
              child:
              ButtonTheme(
                minWidth: 500.0 * SizeConfig.responseWidthRate,
                height: 70.0 * SizeConfig.responseHeightRate,
                child: RaisedButton(onPressed: () {
                  Navigator.push(
                      context, FadeRoute(page: BaseViewController(
                      VialsPage())));
                },
                  child: Text('Number of vials',
                    style: TextStyle(
                        fontFamily: defaultBoldFontFamily,
                        fontWeight: FontWeight.w600,
                        color: BottomButtonColor),
                    textAlign: TextAlign.center,),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(side: BorderSide(color: BottomButtonColor)),
                ),
              ),
            ),
          ],
    ),

        ),
      ),
    );
  }

  Widget doseImageCardList() {

    SingletonCalculatorShareData data = SingletonCalculatorShareData();
    List<dynamic> excelList = data.getSelectWeekDataList();

    List<Widget> list = new List<Widget>();
    String topTitleText = data.getPrepareSyringeString(excelList);
    list.add(Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[ SizedBox(
            width: 40 * SizeConfig.responseWidthRate,
            child:  Container()
        ),TileText(topTitleText,
        SizeConfig.leftPadding, SizeConfig.leftPadding)]));

    for (var i = 1; i < 6; i++) {
      if (data.getStringFromInjectionNumber(excelList,i) != null) {
        var injectionList = data.getInjectionList(excelList, i);
        var nextInjectionList = data.getInjectionList(excelList, (i+1 > 5) ? 5 : i+1);

        for (var j = 0; j < injectionList.length; j = j + 2) {
          double injectionTopPadding = (j == 0 && i == 1 &&
              topTitleText.contains(",")) ? 30 : 40;
          String injectionNumberString = (j == 0) ? data
              .getStringFromInjectionNumber(excelList, i) : "";

          if (injectionList[j] == DoseType.SkyBlue) {
            list.add(doseImageCardRowWithStepImage(childWidget: DoseImageCard(
              doseImageCardType: DoseImageCardType.Injection,
              drugName: "Sky blue",
              imagePath: "assets/images/Skyblue_card2.png",
              cardIndex: i,
              doseML: injectionList[j + 1],
              injectionNumberString: injectionNumberString
              ,
              injectionTopPadding: injectionTopPadding,
            ),
                injectionTopPadding: injectionTopPadding,
                showImage: (j == 0), stepNumber: i, isFinalStep: nextInjectionList.length == 0));
          } else if (injectionList[j] == DoseType.Purple) {
            list.add(doseImageCardRowWithStepImage(childWidget: DoseImageCard(
              doseImageCardType: DoseImageCardType.Injection,
              drugName: "Purple",
              imagePath: "assets/images/Purple_card2.png",
              cardIndex: i,
              doseML: injectionList[j + 1],
              injectionNumberString: injectionNumberString,
              injectionTopPadding: injectionTopPadding,
            ),
                injectionTopPadding: injectionTopPadding,
                showImage: (j == 0), stepNumber: i, isFinalStep: nextInjectionList.length == 0));
          } else if (injectionList[j] == DoseType.Green) {
            list.add(
                doseImageCardRowWithStepImage(childWidget: DoseImageCard(
                  doseImageCardType: DoseImageCardType.Injection,
                  drugName: "Turquoise",
                  imagePath: "assets/images/Green_card2.png",
                  cardIndex: i,
                  doseML: injectionList[j + 1],
                  injectionNumberString: injectionNumberString,
                  injectionTopPadding: injectionTopPadding,
                ),
                    injectionTopPadding: injectionTopPadding,
                    showImage: (j == 0), stepNumber: i, isFinalStep: nextInjectionList.length == 0));
          } else if (injectionList[j] == DoseType.Brown) {
            list.add(doseImageCardRowWithStepImage(childWidget: DoseImageCard(
              doseImageCardType: DoseImageCardType.Injection,
              drugName: "Brown",
              imagePath: "assets/images/Brown_card2.png",
              cardIndex: i,
              doseML: injectionList[j + 1],
              injectionNumberString: injectionNumberString,
              injectionTopPadding: injectionTopPadding,
            ),
                injectionTopPadding: injectionTopPadding,
                showImage: (j == 0), stepNumber: i, isFinalStep: nextInjectionList.length == 0));

          }
        }
      }
    }

    return new Column(children: list ,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
    );

  }

  Widget doseImageCardRowWithStepImage({Widget childWidget,double injectionTopPadding,bool showImage,int stepNumber,bool isFinalStep}) {

    double mySeparatorHeight = 170.0 * SizeConfig.responseHeightRate;
    return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              (stepNumber == 1 && showImage||isFinalStep && !showImage) ? Container(height: injectionTopPadding): MySeparator(height: injectionTopPadding),
             SizedBox(
                    width: 40 * SizeConfig.responseWidthRate,
                    child: showImage ? Image.asset(
                      "assets/images/Step$stepNumber.png",
                      fit: BoxFit.fitHeight,
                    ) : Container()
                ),

              isFinalStep ? Container(height: mySeparatorHeight): MySeparator(height: mySeparatorHeight)

            ],
          ),
          childWidget
        ]);
  }

}

class TileText extends StatelessWidget {

  final String text;
  final double topPadding;
  final double leftPadding;

  TileText(this.text,this.topPadding,this.leftPadding);

  Widget build(context) {
    SizeConfig().init(context);

    return Padding(
            padding: EdgeInsets.only(
                left: leftPadding,
                right: SizeConfig.insideLeftPadding * 2,
                top:(text.contains(',')) ? 0 : topPadding
            ),
            child: Text(
                text,
                overflow: TextOverflow.ellipsis,
                maxLines: 5, style: TextStyle(
                  color: DefaultBlueTextColor,
                  fontFamily: defaultBoldFontFamily,
                  fontWeight: FontWeight.w600,
                  fontSize: 18.0 *
                      SizeConfig.responseFrontSizeRate,
                  wordSpacing: 0.0
              ),
              ),

        );
  }
}

class MySeparator extends StatelessWidget {
  final double height;

  const MySeparator({this.height});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: height,
        child:LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainHeight();
        final dashWidth = 1.0;
        final dashHeight = height;
        final dashCount = (height / (2.5 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashWidth,
              child: DecoratedBox(
                decoration: BoxDecoration(color: OrangeColor),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.vertical,
        );
      },
    )
    );
  }
}
