import 'package:calculator/controller/baseViewController.dart';
import 'package:calculator/model/singletonCalculatorShareData.dart';
import 'package:calculator/utilities/fade_route.dart';
import 'package:calculator/utilities/sizeConfig.dart';
import 'package:calculator/utilities/style.dart';
import 'package:calculator/view/InjectionPage.dart';
import 'package:calculator/view/topBarImage.dart';
import 'package:flutter/material.dart';

import 'doseImageCard.dart';
import 'doseImageCardList.dart';


class DosePage extends StatefulWidget {
  static const String routeName = "/dose";
  @override
  _DosePageState createState() => _DosePageState();
}

class _DosePageState extends State<DosePage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SingletonCalculatorShareData data = SingletonCalculatorShareData();
    if (!data.isMaintenanceDose) {
      data.selectedCard = true;
      data.selectedCardIndex = 1;
    }
  }

  @override
  Widget build(BuildContext context) {



    return Scaffold(
      backgroundColor: Colors.white,
      appBar: TopBarImage(TopBarType.DosePage),
      body: SafeArea(
        top: true,
        bottom: false,
        child: SingleChildScrollView(
    child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children:dosePage(context),
        ),
        ),
      ),
    );
  }

  List<Widget> dosePage(BuildContext context) {
    List<Widget> widgetList = List();
    SizeConfig().init(context);
    final InheritedContext inheritedContext = InheritedContext.of(context);
    final data = SingletonCalculatorShareData();
    var kg = data.kg;
    widgetList.add(Padding(
      padding: EdgeInsets.only(
          left: SizeConfig.leftPadding,
          right: SizeConfig.leftPadding,
          top: 5 * SizeConfig.responseHeightRate),
      child: Container(
          height: 60 * SizeConfig.responseHeightRate,
          width: SizeConfig.screenWidth,
          decoration: new BoxDecoration(
              color: TopTextBackGroundGreyColor,
              borderRadius: new BorderRadius.circular(3.0)),
          child: Center(
              child: Text(
                data.isMaintenanceDose ? 'Maintenance Dose, $kg' + 'kg' : 'Loading Dose, $kg' + 'kg',
                style: TextStyle(
                  backgroundColor: TopTextBackGroundGreyColor,
                  color: DefaultBlueTextColor,
                  fontFamily: defaultBoldFontFamily,
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0 * SizeConfig.responseFrontSizeRate,
                ),
                textAlign: TextAlign.center,
              )
          )
      ),
    )
    );
    if(data.isMaintenanceDose) {
      widgetList.add(
          (data.selectedCard && data.selectedCardIndex == 1)
              ? ExpandDoseCard(
              "QW", (kg * 1.5).toString() + "mg", inheritedContext, 1)
              : DoseCard(
              "QW", (kg * 1.5).toString() + "mg", inheritedContext, 1));
      widgetList.add(
          (data.selectedCard && data.selectedCardIndex == 2)
              ? ExpandDoseCard(
              "Q2W", (kg * 3).toString() + "mg", inheritedContext, 2)
              : DoseCard(
              "Q2W", (kg * 3).toString() + "mg", inheritedContext, 2));
      widgetList.add(
          (data.selectedCard && data.selectedCardIndex == 3)
              ? ExpandDoseCard(
              "Q4W", (kg * 6).toString() + "mg", inheritedContext, 3)
              : DoseCard(
              "Q4W", (kg * 6).toString() + "mg", inheritedContext, 3));
      widgetList.add(Padding(
        padding:  EdgeInsets.only(left: SizeConfig.leftPadding+SizeConfig.insideLeftPadding,top: SizeConfig.leftPadding+SizeConfig.insideLeftPadding ,bottom: SizeConfig.leftPadding+SizeConfig.insideLeftPadding * 3),
        child: Row
          (mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          SizedBox(
              height: 45.0 * SizeConfig.responseHeightRate,
              child: Image.asset(
                  "assets/images/LeastWastage.png",
                  fit: BoxFit.fitHeight,
                ),

              )
        ]),
      ));
    } else {
      widgetList.add(
          (data.selectedCard && data.selectedCardIndex == 1)
              ? ExpandDoseCard(
              "QW", (kg * 3).toString() + "mg", inheritedContext, 1)
              : DoseCard(
              "QW", (kg * 3).toString() + "mg", inheritedContext, 1));
      widgetList.add(Padding(
          padding:  EdgeInsets.only(left: SizeConfig.leftPadding+SizeConfig.insideLeftPadding,top: SizeConfig.leftPadding+SizeConfig.insideLeftPadding ,bottom: SizeConfig.leftPadding+SizeConfig.insideLeftPadding * 3),
    child:Container()));
    }


    return widgetList;

  }
}


class DoseCard extends StatefulWidget {

  final String leftText;
  final String rightText;
  final int selectedCardIndex;
  final InheritedContext inheritedContext;
  DoseCard(this.leftText,this.rightText,this.inheritedContext,this.selectedCardIndex);
  @override
  _DoseCardState createState() => _DoseCardState();
}

class _DoseCardState extends State<DoseCard> {


  _DoseCardState();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return GestureDetector(
        // When the child is tapped, show a snackbar.
        onTap: () {
          widget.inheritedContext.selectCard(widget.selectedCardIndex);
    },
    // The custom button
    child: Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.leftPadding,
              right: SizeConfig.leftPadding,
              top: 30 * SizeConfig.responseHeightRate),
          child: Card(
              elevation: 10,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(15.0),
                    bottomRight: const Radius.circular(15.0)),
              ),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image(image: AssetImage('assets/images/LeftLine.png')),
                    Padding(
                        padding: EdgeInsets.only(
                            left: SizeConfig.insideLeftPadding,
                            right: SizeConfig.insideLeftPadding),
                        child: Container(
                            height: 120 * SizeConfig.responseHeightRate,
                            child:
                            Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        widget.leftText,
                                        style: TextStyle(
                                          backgroundColor: Colors.white,
                                          color: DefaultBlueTextColor,
                                          fontFamily: defaultBoldFontFamily,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 20.0 *
                                              SizeConfig.responseFrontSizeRate,
                                        ),
                                      ),
                                      WeekTextRowWidget(widget.selectedCardIndex),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Text(
                                        widget.rightText,
                                        style: TextStyle(
                                          backgroundColor: Colors.white,
                                          color: OrangeColor,
                                          fontFamily: defaultBoldFontFamily,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 30.0 *
                                              SizeConfig.responseFrontSizeRate,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left:SizeConfig.leftPadding / 2),
                                        child: SizedBox(
                                            width: 15.0 *
                                                SizeConfig.responseWidthRate,
                                            child: Image.asset(
                                              "assets/images/DownArrow.png",
                                              fit: BoxFit.fitWidth,
                                            )),
                                      )
                                    ],
                                  )
                                ])
                        )),
                    Image(image: AssetImage('assets/images/RightLine.png'))
                  ]
              )
          )
      ));
  }
}

class ExpandDoseCard extends StatefulWidget {

  final String leftText;
  final String rightText;
  final int selectedCardIndex;
  final InheritedContext inheritedContext;
  ExpandDoseCard(this.leftText,this.rightText,this.inheritedContext,this.selectedCardIndex);

  @override
  _ExpandDoseCard createState() => _ExpandDoseCard();
}

class _ExpandDoseCard extends State<ExpandDoseCard> {

    @override
    Widget build(BuildContext context) {
      SingletonCalculatorShareData data = SingletonCalculatorShareData();
      SizeConfig().init(context);

      return GestureDetector(
        // When the child is tapped, show a snackbar.
          onTap: () {
            widget.inheritedContext.selectCard(widget.selectedCardIndex);
          },
          // The custom button
          child: Padding(
              padding: EdgeInsets.only(
                  left: SizeConfig.leftPadding,
                  right: SizeConfig.leftPadding,
                  top: 30 * SizeConfig.responseHeightRate),
              child: Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(15.0),
                        bottomRight: const Radius.circular(15.0)),
                  ),
                  child:
                  Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Image(image: AssetImage('assets/images/LeftLine.png')),


                        Padding(
                            padding: EdgeInsets.only(
                                left: SizeConfig.insideLeftPadding,
                                right: SizeConfig.insideLeftPadding),
                            child: Container(
                                height: 120 * SizeConfig.responseHeightRate,
                                child:
                                Row(
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            widget.leftText,
                                            style: TextStyle(
                                              backgroundColor: Colors.white,
                                              color: DefaultBlueTextColor,
                                              fontFamily: defaultBoldFontFamily,
                                              fontWeight: FontWeight.w600,
                                              fontSize: 20.0 *
                                                  SizeConfig.responseFrontSizeRate,
                                            ),
                                          ),
                                          WeekTextRowWidget(data.selectedCardIndex),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: <Widget>[
                                          Text(
                                            widget.rightText,
                                            style: TextStyle(
                                              backgroundColor: Colors.white,
                                              color: OrangeColor,
                                              fontFamily: defaultBoldFontFamily,
                                              fontWeight: FontWeight.w600,
                                              fontSize: 30.0 *
                                                  SizeConfig.responseFrontSizeRate,
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(left:SizeConfig.leftPadding / 2),
                                            child: SizedBox(
                                                width: 15.0 *
                                                    SizeConfig.responseWidthRate,
                                                child: Image.asset(
                                                  "assets/images/UpArrow.png",
                                                  fit: BoxFit.fitWidth,
                                                )),
                                          )
                                        ],
                                      )
                                    ])
                            )),
                        Padding(
                            padding: EdgeInsets.only(
                                left: SizeConfig.leftPadding,
                                right: SizeConfig.leftPadding),
                            child: Container(
                              color: dividerColor,
                              height: 1,
                            )
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.insideLeftPadding,
                              right: SizeConfig.insideLeftPadding * 5,
                              top: 30 * SizeConfig.responseHeightRate),
                          child: Text(
                            "Numbers of vials required per each administration:",
                            style: TextStyle(
                              backgroundColor: Colors.white,
                              color: DefaultBlueTextColor,
                              fontFamily: defaultBoldFontFamily,
                              fontWeight: FontWeight.w600,
                              fontSize: 15.0 *
                                  SizeConfig.responseFrontSizeRate,
                            ),
                          ),
                        ),
                       DoseImageCardList().createUI(DoseImageCardType.Dose),
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.insideLeftPadding,
                              right: SizeConfig.insideLeftPadding,
                              top: SizeConfig.insideLeftPadding,
                              bottom:SizeConfig.insideLeftPadding),
                          child:
                          ButtonTheme(

                            height: 70.0 * SizeConfig.responseHeightRate,
                            child: RaisedButton(onPressed: () {
                              Navigator.push(
                                  context, FadeRoute(page: BaseViewController(
                                  InjectionPage())));
                            },
                              child: Text('Volume of Injection',
                                style: TextStyle(
                                    fontFamily: defaultBoldFontFamily,
                                    fontWeight: FontWeight.w600,
                                    color: BottomButtonColor),
                                textAlign: TextAlign.center,),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(side: BorderSide(color: BottomButtonColor)),
                            ),
                          ),
                        ),
                        Image(image: AssetImage('assets/images/RightLine.png'))
                      ]
                  )

              )
          ));
    }

  }


class WeekTextRowWidget extends StatelessWidget {

  final int indexCard;

  WeekTextRowWidget(this.indexCard);
  @override
  Widget build(context) {
    SingletonCalculatorShareData data = SingletonCalculatorShareData();
    List<dynamic> excelList;
    if(indexCard == 1) {
      excelList = data.oneWeeksMaintenanceExcelList;
    } else if (indexCard == 2){
      excelList = data.twoWeeksMaintenanceExcelList;
    } else if (indexCard == 3){
      excelList = data.fourWeeksMaintenanceExcelList;
    }

    return Padding(
      padding:  EdgeInsets.only(left: SizeConfig.leftPadding),
      child: data.isLeastWastageChoice(excelList)
          ? SizedBox(
          height: 45.0 *
              SizeConfig.responseHeightRate,
          child: Image.asset(
            "assets/images/LeastWastageIcon.png",
            fit: BoxFit.fitHeight,
          ))
          : Container(),
    );
  }
}


