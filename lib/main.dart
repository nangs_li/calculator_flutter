import 'package:calculator/controller/baseViewController.dart';
import 'package:calculator/utilities/sizeConfig.dart';
import 'package:calculator/utilities/fade_route.dart';
import 'package:calculator/utilities/style.dart';
import 'package:calculator/view/dosePage.dart';
import 'package:calculator/view/topBarImage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vibrate/vibrate.dart';
import 'model/singletonCalculatorShareData.dart';


void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MaterialApp(debugShowCheckedModeBanner: false,home: BaseViewController(MyApp())));
}

typedef ActionCallBack = void Function(Key key);
typedef KeyCallBack = void Function(Key key, BuildContext context);


Key _sevenKey = Key('seven');
Key _eightKey = Key('eight');
Key _nineKey = Key('nine');
Key _fourKey = Key('four');
Key _fiveKey = Key('five');
Key _sixKey = Key('six');
Key _oneKey = Key('one');
Key _twoKey = Key('two');
Key _threeKey = Key('three');
//Key _dotKey = Key('dot');
Key _leftZeroKey = Key('leftzero');
Key _zeroKey = Key('zero');
Key _rightZeroKey = Key('rightzero');
//Key _clearKey = Key('clear');
Key _allClearKey = Key('allclear');
Key _calculateKey = Key('calculate');
Key _calculateTopKey = Key('topcalculate');
Key _calculateBottomKey = Key('bottomcalculate');

class MyApp extends StatefulWidget {
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  List _currentValues = List();
  double lastValue;
  TextEditingController _textEditingController;
  bool savedLastValue = false;


  void  onKeyTapped(Key key, BuildContext context) {

    InheritedContext inheritedContext = InheritedContext.of(context);
    SingletonCalculatorShareData data = SingletonCalculatorShareData();
    if(data.maintenanceExcelList.length == 0){
      return;
    }

    if (savedLastValue == false && lastValue != null) {
      _currentValues.clear();
      savedLastValue = true;
    }
    setState(() {
      Vibrate.feedback(FeedbackType.medium);
      HapticFeedback.mediumImpact();

      if (identical(_sevenKey, key)) {
        _currentValues.add('7');
      } else if (identical(_eightKey, key)) {
        _currentValues.add('8');
      } else if (identical(_nineKey, key)) {
        _currentValues.add('9');
      } else if (identical(_fourKey, key)) {
        _currentValues.add('4');
      } else if (identical(_fiveKey, key)) {
        _currentValues.add('5');
      } else if (identical(_sixKey, key)) {
        _currentValues.add('6');
      } else if (identical(_oneKey, key)) {
        _currentValues.add('1');
      } else if (identical(_twoKey, key)) {
        _currentValues.add('2');
      } else if (identical(_threeKey, key)) {
        _currentValues.add('3');
//      }
//      else if (identical(_dotKey, key)) {
//        if (!_currentValues.contains('.')) {
//          _currentValues.add('.');
//        }
      } else if (identical(_zeroKey, key) || identical(_leftZeroKey, key) || identical(_rightZeroKey, key)) {
        if(int.parse(convertToString(_currentValues)) != 0) {
          _currentValues.add('0');
        }
//      } else if (identical(_clearKey, key)) {
//        print('Values :: $_currentValues');
//        if(_currentValues.length > 0) {
//          _currentValues.removeLast();
//        }

      } else if (identical(_allClearKey, key)) {
        _currentValues.clear();
        lastValue = null;
        savedLastValue = false;
        _textEditingController.clear();
      } else if (identical(_calculateTopKey, key) || identical(_calculateKey, key) || identical(_calculateBottomKey, key)) {
        var kg = int.parse(convertToString(_currentValues));
        if(kg > 2) {
          data.getWeeksExcelList(kg);
          Navigator.push(context, FadeRoute(page: BaseViewController(DosePage())));
        } else {
          showDialogMessage("Please enter bigger than 2 kg");
        }

      }


      if(int.parse(convertToString(_currentValues)) <= 150 || _currentValues.length == 0){

      } else {
        _currentValues.removeLast();
        showDialogMessage("Please enter smaller than 150 kg");
      }
      _textEditingController.text =  convertToString(_currentValues) + " kg";




    });
  }

  String validateDouble(double doubleValue) {
    int value;
    if (doubleValue % 1 == 0) {
      value = doubleValue.toInt();
    } else {
      return doubleValue.toStringAsFixed(1);
    }
    return value.toString();
  }

  String convertToString(List values) {
    String val = '';
    for (int i = 0; i < values.length; i++) {
      val += _currentValues[i];
    }
    if(values.length == 0) return '0';
    return val;
  }

  List convertToList(String value) {
    List list = new List();
    for (int i = 0; i < value.length; i++) {
      list.add(String.fromCharCode(value.codeUnitAt(i)));
    }
    return list;
  }

  void changeMaintenanceDose() {
    setState(() {
      SingletonCalculatorShareData data = SingletonCalculatorShareData();
      data.isMaintenanceDose = !data.isMaintenanceDose;
    });
  }

  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
    _textEditingController.text = "0 kg";

  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);


    final InheritedContext inheritedContext = InheritedContext.of(context);
    final data = SingletonCalculatorShareData();
    if(data.allClearData){
      onKeyTapped(_allClearKey,context);
      print("123QQ");
      data.allClearData = false;
    }
    if(data.maintenanceExcelList.length == 0) {
      data.loadMaintenanceCSV();
      data.loadLoadingCSV();
    }


    return Scaffold(
      backgroundColor: Colors.white,
      appBar:  TopBarImage(TopBarType.homePage),
      body:  Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            children: <Widget>[
              Row(mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                            top: 55 * SizeConfig.responseHeightRate,
                          ),
                          child:new Text(
                        data.isMaintenanceDose ? 'Weeks 5+' : 'Weeks 1-4',
                        style: new TextStyle(fontSize: 18.0 * SizeConfig.responseFrontSizeRate ,
                            fontFamily: defaultFontFamily,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w600,
                            color: OrangeColor),
                      ),
                      ),
                    ]),
              GestureDetector(
                // When the child is tapped, show a snackbar.
                onTap: () {
                  changeMaintenanceDose();
                },
                // The custom button
                child: Row(mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[

                      new Text(
                        (data.isMaintenanceDose) ?'Maintenance Dose' : 'Loading Dose',
                        style:  BoldTextStyle.copyWith(fontSize: 35 * SizeConfig.responseFrontSizeRate,color: OrangeColor),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                          height: 45.0 * SizeConfig.responseHeightRate,
                          width: 30.0 * SizeConfig.responseWidthRate,
                          child: IconButton(
                            icon: Image.asset(
                              "assets/images/Filter.png",
                              fit: BoxFit.fitWidth,
                            ),

                          ))
                    ]),
              )

            ],
          ),
          Padding(
            padding: EdgeInsets.only(
            bottom: 15 * SizeConfig.responseHeightRate,
          ),
          child:Container(
            alignment: Alignment.center,
            width: SizeConfig.screenWidth,
            height: SizeConfig.safeBlockVertical * 15,
            child: IgnorePointer(
              child: TextField(
                enabled: false,
                autofocus: false,
                controller: _textEditingController,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: defaultBoldFontFamily,
                  fontStyle: FontStyle.normal,
                  color: DefaultBlueTextColor,
                  fontWeight: FontWeight.w600,
                  fontSize: 40.0 * SizeConfig.responseFrontSizeRate,
                ),
                decoration: InputDecoration.collapsed(

                ),
              ),
            ),
          ),
        ),
          Expanded(
            child: Container(
             color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SizedBox(width: SizeConfig.leftPadding),
                        buildKeyItem('7', _sevenKey,context),
                        buildKeyItem('8', _eightKey,context),
                        buildKeyItem('9', _nineKey,context),
                        buildKeyItem('C', _allClearKey,context),
                        SizedBox(width: SizeConfig.leftPadding),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SizedBox(width: SizeConfig.leftPadding),
                        buildKeyItem('4', _fourKey,context),
                        buildKeyItem('5', _fiveKey,context),
                        buildKeyItem('6', _sixKey,context),
                        buildKeyItem('', _calculateTopKey,context),
                        SizedBox(width: SizeConfig.leftPadding),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SizedBox(width: SizeConfig.leftPadding),
                        buildKeyItem('1', _oneKey,context),
                        buildKeyItem('2', _twoKey,context),
                        buildKeyItem('3', _threeKey,context),
                        buildKeyItem('Calculate', _calculateKey,context),
                        SizedBox(width: SizeConfig.leftPadding),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(width: SizeConfig.leftPadding),
                        buildKeyItem('', _leftZeroKey,context),
                        buildKeyItem('0', _zeroKey,context),
                        buildKeyItem('', _rightZeroKey,context),
                        buildKeyItem('', _calculateBottomKey,context),
                        SizedBox(width: SizeConfig.leftPadding),
                      ],
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(top: SizeConfig.leftPadding/2),
                      child: Container()),
                  Padding(
                      padding: EdgeInsets.only(top: 60.0 * SizeConfig.responseHeightRate),
                      child: Container()),
                ],
              ),
            ),
          )
        ],

      ),
    );
  }

  KeyItem buildKeyItem(String val, Key key, BuildContext context) {
    return KeyItem(
      key: key,
      child: Text(
        val,
        style: TextStyle(
          color: ((key == _calculateKey) ? Colors.white : DefaultBlueTextColor),
          fontFamily: defaultBoldFontFamily,
          fontStyle: FontStyle.normal,
          fontSize: ((key == _calculateKey) ? 22 : 45.0 ) * SizeConfig.responseHeightRate,
          fontWeight: FontWeight.w600,
        ),
      ),
      onKeyTap: onKeyTapped,
    );
  }

  void showDialogMessage(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(message,style: TextStyle(color: DefaultBlueTextColor),),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

class KeyItem extends StatelessWidget {
  final Widget child;
  final Key key;
  final KeyCallBack onKeyTap;

  KeyItem({@required this.child, this.key, this.onKeyTap});


  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterial(context));
    final InheritedContext inheritedContext = InheritedContext.of(context);

    return Expanded(
      child: Material(
        type: MaterialType.transparency,
        child: InkResponse(
          splashColor:  Colors.white ,
          highlightColor: Colors.white,
          onTap: () => onKeyTap(key,context),
          child: Container(
            decoration: BoxDecoration(border: getBoxBorder(key),color: ((key == _calculateKey) || (key == _calculateTopKey) || (key == _calculateBottomKey)  ? calculateButtonColor : null )),
            alignment: Alignment.center,
            child: child,
          ),
        ),
      ),
    );
  }

  BoxBorder getBoxBorder(Key key) {
    if (key == Key('three') || key == Key('six') || key == Key('nine')) {
      return Border(
          left: BorderSide(width: 0.5, color: dividerColor),
          bottom: BorderSide(width: 0.5, color: dividerColor));
    } else if (key == Key('one') || key == Key('four') || key == Key('seven')) {
      return Border(
          right: BorderSide(width: 0.5, color: dividerColor),
          bottom: BorderSide(width: 0.5, color: dividerColor));
    } else if (key == Key('eight') || key == Key('five') || key == Key('two')) {
      return Border(bottom: BorderSide(width: 0.5, color: dividerColor));
    } else if (key == Key('allclear')) {
      return Border(left: BorderSide(width: 0.5, color: dividerColor));
    } else if (key == Key('dot')) {
      return Border(right: BorderSide(width: 0.5, color: dividerColor));
    }
  }
}
