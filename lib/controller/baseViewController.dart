
import 'package:calculator/model/singletonCalculatorShareData.dart';
import 'package:flutter/material.dart';

class BaseViewController extends StatefulWidget {
  Widget childWidget;

  BaseViewController(this.childWidget, { Key key }) : super(key: key);
  @override


  _BaseViewControllerState createState() => new _BaseViewControllerState();

}

class _BaseViewControllerState
    extends State<BaseViewController> {

  _initData() {
    //SingletonShareData = new SingletonShareData(selected:false,textEditingController: TextEditingController(),useCustomAppBar: false,animationFinish: false);
  }

  @override
  void initState() {
    _initData();
    super.initState();
  }


  void selectCard(selectedCardIndex) {
    SingletonCalculatorShareData data = SingletonCalculatorShareData();

    setState(() {
      if(data.selectedCardIndex == selectedCardIndex || data.selectedCard == false){
        data.selectedCard = !data.selectedCard;
      }
      data.selectedCardIndex = selectedCardIndex;

    });

  }
  //calculator end

  @override
  Widget build(BuildContext context) {

    return new InheritedContext(
        selectCard:selectCard,
        child: widget.childWidget);
  }
}

class InheritedContext extends InheritedWidget {

  //searchbar Animation part
  final Function() searchPressed;
  final Function() changeCustomAppBar;
  //searchbar Animation part end

  //Calculator part
//  final Function(SingletonCalculatorShareData, dynamic ,dynamic) getWeeksExcelList;
//  final Function(List <dynamic> ,dynamic ,SingletonCalculatorShareData) testGettingExcelDataCorrect;
//  final Function(List <dynamic>) get30mgDrugName;
  final Function(dynamic) selectCard;
  //Calculator part end


  InheritedContext({
    Key key,
    @required this.selectCard,
    @required this.searchPressed,
    @required this.changeCustomAppBar,
    @required Widget child,
  }) : super(key: key, child: child);

  static InheritedContext of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(InheritedContext);
  }

  //是否重建widget就取决于数据是否相同
  @override
  bool updateShouldNotify(InheritedContext oldWidget) {
    return true;
  }
}






